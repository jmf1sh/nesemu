#include "status.hpp"
#include <cstddef>

using namespace nesemu;

u8 Status::pack() const {
  u8 bits = 0;
  for (size_t i = 0; i < 8; ++i) {
    bits |= flags[i] << i;
  }
  return bits;
}

void Status::unpack(u8 bits) {
  for (size_t i = 0; i < 8; ++i) {
    flags[i] = bits & (1 << i);
  }
}

void Status::set_SZ(u8 val) {
  (*this)[Flag::S] = val & 0x80;
  (*this)[Flag::Z] = val == 0;
}
