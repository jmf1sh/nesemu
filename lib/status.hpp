#ifndef NESEMU_STATUS_HPP_INCLUDED
#define NESEMU_STATUS_HPP_INCLUDED

#include "types.hpp"

namespace nesemu {
//! Flags for status register
enum class Flag : u8 {
  //! Carry
  C = 0,
  //! Zero
  Z = 1,
  //! Interrupt disable
  I = 2,
  //! Decimal
  D = 3,
  //! Software interrupt
  B = 4,
  //! Unused. Should always be 1
  U = 5,
  //! Overflow
  V = 6,
  //! Sign (1 if result negative)
  S = 7
};

struct Status {
  bool flags[8];

  inline bool &operator[](Flag f) { return flags[u8(f)]; }

  u8 pack() const;
  void unpack(u8 bits);

  void set_SZ(u8 val);
};

} // namespace nesemu

#endif
