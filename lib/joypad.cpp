#include "joypad.hpp"

#include <iostream>

using namespace nesemu;

static const u16 JOYPAD0 = 0x4016;
static const u16 JOYPAD1 = 0x4017;

JoyPad::JoyPad() {
  index = 0;
  strobe = false;

  for (auto i = 0; i < 8; ++i) {
    state[0][i] = false;
    state[1][i] = false;
  }
}

u8 JoyPad::peek(u16 addr) {
  // invalid address!
  if (addr != JOYPAD0 && addr != JOYPAD1) {
    return 0;
  }

  u8 joy_index = (addr - JOYPAD0);

  if (strobe) {
    return state[joy_index][0];
  }

  u8 val = state[joy_index][index];

  std::cout << "joypad read " << i32(joy_index) << " " << i32(index) << " "
            << i32(val) << std::endl;

  index += 1;
  index %= 8;

  return val; // | 0x40; // ??
}

void JoyPad::poke(u16 addr, u8 val) {
  if (addr != JOYPAD0) {
    return;
  }

  strobe = (val & 1);
  index = 0;
}
