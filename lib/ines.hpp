#ifndef NESEMU_INES_HPP_INCLUDED
#define NESEMU_INES_HPP_INCLUDED

#include "types.hpp"

#include <fstream>
#include <memory>
#include <string>
#include <vector>

namespace nesemu {

constexpr size_t bank_size = 8 * 1024;
constexpr size_t header_size = 16;
constexpr size_t trainer_size = 512;

using rom_bank = u8[bank_size];

struct Mapper {
  std::vector<u8> prg_rom;
  std::vector<u8> prg_ram;
  std::vector<u8> chr_rom;

  virtual ~Mapper() = default;
  virtual u8 peek(u16 addr) = 0;
  virtual void poke(u16 addr, u8 val) = 0;
};

struct NesHeader {
  u8 nes[4];
  u8 n_prg_rom;
  u8 n_chr_rom;
  u8 flags6;
  u8 flags7;

  u8 n_prg_ram;
  u8 flags9;
  u8 flags10;
  u8 fill[5];
};

struct NesRom {
  NesHeader header;
  u8 mapper_id;
  std::unique_ptr<Mapper> mapper;
  Mirroring mirror_mode;

  void open(std::ifstream &s);
};
} // namespace nesemu

#endif
