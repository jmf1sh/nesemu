#include <iostream>

#include "apu.hpp"
#include "cpu.hpp"
#include "ines.hpp"
#include "joypad.hpp"
#include "ppu.hpp"
#include "system.hpp"

using namespace nesemu;

System::System() : ram(ram_size), rom(), apu(this), ppu(this), cpu(this) {}

//! Helper function for NES memory map.
//! See https://wiki.nesdev.com/w/index.php/CPU_memory_map
template <bool write> u8 mem_access(System *sys, u16 addr, u8 val = 0) {

  // internal RAM
  if (addr <= 0x1fff) {
    // higher address are mirrors of ram
    addr %= 0x0800;
    u8 *ptr = reinterpret_cast<u8 *>(&sys->ram[addr]);
    if (write) {
      *ptr = val;
    }
    return *ptr;
  }
  // PPU
  else if (addr >= 0x2000 && addr <= 0x3fff) {
    // mirror
    addr = 0x02000 + ((addr - 0x2000) % 8);
    if (write) {
      sys->ppu.poke(addr, val);
      return val;
    } else {
      return sys->ppu.peek(addr);
    }
  }
  // APU and IO
  else if (addr >= 0x4000 && addr <= 0x4017) {
    // todo

    // controller
    // https://wiki.nesdev.com/w/index.php/Controller_Reading
    if (addr == 0x4016 || addr == 0x4017) {
      if (write) {
        sys->joypad.poke(addr, val);
        return val;
      }
      return sys->joypad.peek(addr);
    }

    // OAMDMA
    if (addr == 0x4014) {
      sys->ppu.poke(addr, val);
      return val;
    }

    return val;
  }
  // CPU test mode
  else if (addr >= 0x4018 && addr <= 0x401f) {
    // todo
    return val;
  }
  // cartridge rom and mapper registers
  else if (addr >= 0x4020 && addr <= 0xffff) {
    if (write) {
      sys->rom.mapper->poke(addr, val);
      return val;
    } else {
      return sys->rom.mapper->peek(addr);
    }
  }

  // we should never reach here!
  return val;
}

u8 System::peek(u16 addr) { return mem_access<false>(this, addr); }

u16 System::peekw(u16 addr) {
  u8 lo = peek(addr);
  u8 hi = peek(addr + 1);
  return 256 * hi + lo;
}

void System::poke(u16 addr, u8 val) { mem_access<true>(this, addr, val); }

void System::step_frame() {
  bool odd_frame = ppu.odd_frame;
  while (odd_frame == ppu.odd_frame) {
    cpu.exec();
  }
}

void System::reset() {
  cpu.reset();
  ppu.reset();
}
