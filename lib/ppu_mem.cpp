#include <iomanip>
#include <iostream>

#include "ppu_mem.hpp"

using namespace nesemu;

PPUMemory::PPUMemory() {}

u8 &PPUMemory::operator[](u16 addr) {
  // PPU limited to 0x4000 bytes
  addr %= 0x4000;

  // pattern table?
  if (addr < 0x2000) {
    auto index = addr / 0x1000;
    auto offset = addr % 0x1000;
    return pattern[index][offset];
  }

  // nametable?
  if (addr < 0x3F00) {
    addr -= 0x2000;
    // mirroring in range 0x3000 to 0x3F00
    addr %= 0x1000;

    u8 index = addr / 0x400;
    u16 offset = addr % 0x400;

    return nametables[index][offset];
  }

  // remaining case: palette
  addr -= 0x3F00;
  addr %= 0x0020;

  // background color mirroring
  /*
  if (addr >= 0x10 && (addr%4) == 0) {
    addr -= 0x10;
    }*/

  /*if (addr >= 0x10) {
    if ((addr%4) != 0) {
      addr -= 0x10;
    } else {
      addr = 0;
    }
    }*/

  if (addr == 0x10 || addr == 0x14 || addr == 0x18 || addr == 0x1C) {
    addr -= 0x10;
  }

  return palette[addr];
}

void PPUMemory::set_mirror_mode(Mirroring mode) {
  switch (mode) {
  case Mirroring::HORIZONTAL:
    nametables[0] = &nametable_buffer[0];
    nametables[1] = &nametable_buffer[0];
    nametables[2] = &nametable_buffer[0x400];
    nametables[3] = &nametable_buffer[0x400];
    break;
  case Mirroring::VERTICAL:
    nametables[0] = &nametable_buffer[0];
    nametables[1] = &nametable_buffer[0x400];
    nametables[2] = &nametable_buffer[0];
    nametables[3] = &nametable_buffer[0x400];
    break;
  case Mirroring::SINGLE:
    nametables[0] = &nametable_buffer[0];
    nametables[1] = &nametable_buffer[0];
    nametables[2] = &nametable_buffer[0];
    nametables[3] = &nametable_buffer[0];
    break;
  case Mirroring::QUAD:
    nametables[0] = &nametable_buffer[0];
    nametables[1] = &nametable_buffer[0x400];
    nametables[2] = &nametable_buffer[0x800];
    nametables[3] = &nametable_buffer[0xC00];
    break;
  }
}
