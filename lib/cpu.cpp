
#include "cpu.hpp"
#include "addr_mode.hpp"

#include <cassert>
#include <iomanip>
#include <iostream>
#include <sstream>

using namespace nesemu;

// helper function for branching
template <class Mode> void branch(CPU *cpu, Flag flag, bool set) {
  Mode m(cpu);
  u8 ticks = 0;
  if (cpu->status[flag] == set) {
    cpu->IP = m.addr;
    ticks = 3 + m.cross_page();
  } else {
    ++cpu->IP;
    ticks = 2;
  }
  cpu->tick(ticks);
}

//! Helper macro for transfer operations
#define TRANSFER(r1, r2)                                                       \
  cpu->r2 = cpu->r1;                                                           \
  cpu->status.set_SZ(cpu->r2)

#define DECLARE_OP(op)                                                         \
  template <class Mode, size_t ticks> void _##op(nesemu::CPU *cpu)

//! add with carry
DECLARE_OP(ADC) {
  Mode m(cpu);
  u16 c = cpu->status[Flag::C];
  u16 x = cpu->A;
  u16 y = m.peek();
  u16 result = x + y + c;
  bool set_c = result > 0xFF;
  cpu->status[Flag::C] = set_c;

  i16 sx = x > 127 ? x - 256 : x;
  i16 sy = y > 127 ? y - 256 : y;
  i16 sr = sx + sy + c;
  cpu->status[Flag::V] = (sr > 127) || (sr < -128);

  cpu->A = u8(result);
  cpu->status.set_SZ(cpu->A);
  cpu->tick(ticks + m.cross_page());
}

//! bitwise and
DECLARE_OP(AND) {
  Mode m(cpu);
  cpu->A &= m.peek();
  cpu->status.set_SZ(cpu->A);
  cpu->tick(ticks + m.cross_page());
}

//! arithmetic shift left
DECLARE_OP(ASL) {
  Mode m(cpu);
  u8 val = m.peek();
  cpu->status[Flag::C] = val & 0x80;
  val <<= 1;
  m.poke(val);
  cpu->status.set_SZ(val);
  cpu->tick(ticks + m.cross_page());
}

//! Branch if carry clear
DECLARE_OP(BCC) { branch<Mode>(cpu, Flag::C, false); }

//! Branch if carry set
DECLARE_OP(BCS) { branch<Mode>(cpu, Flag::C, true); }

//! Branch if equal
DECLARE_OP(BEQ) { branch<Mode>(cpu, Flag::Z, true); }

// Bitwise compare (todo...)
DECLARE_OP(BIT) {
  Mode m(cpu);
  const u8 val = m.peek();
  cpu->status[Flag::Z] = (cpu->A & val) == 0;
  cpu->status[Flag::S] = val & (1 << 7);
  cpu->status[Flag::V] = val & (1 << 6);
  cpu->tick(ticks + m.cross_page());
}

//! Branch if minus
DECLARE_OP(BMI) { branch<Mode>(cpu, Flag::S, true); }

//! Branch if not equal
DECLARE_OP(BNE) { branch<Mode>(cpu, Flag::Z, false); }

//! Branch if plus
DECLARE_OP(BPL) { branch<Mode>(cpu, Flag::S, false); }

//! Break
DECLARE_OP(BRK) {
  Status status = cpu->status;
  status[Flag::B] = true;
  u8 lo = u8(cpu->IP);
  u8 hi = u8(cpu->IP >> 8);
  cpu->push(hi);
  cpu->push(lo);
  cpu->push(status.pack());
  cpu->IP = cpu->sys->peekw(CPU::irq_vector);
  cpu->tick(ticks);
}

//! Branch if overflow clear
DECLARE_OP(BVC) { branch<Mode>(cpu, Flag::V, false); }

//! Branch if overflow set
DECLARE_OP(BVS) { branch<Mode>(cpu, Flag::V, true); }

//! Clear carry flag
DECLARE_OP(CLC) {
  cpu->status[Flag::C] = false;
  cpu->tick(ticks);
}

//! Clear decimal flag
DECLARE_OP(CLD) {
  cpu->status[Flag::D] = false;
  cpu->tick(ticks);
}

//! Clear interrupt disable flag
DECLARE_OP(CLI) {
  cpu->status[Flag::I] = false;
  cpu->tick(ticks);
}

//! Clear overflow flag

DECLARE_OP(CLV) {
  cpu->status[Flag::V] = false;
  cpu->tick(ticks);
}

//! Compare with A
DECLARE_OP(CMP) {
  Mode m(cpu);
  u8 val = m.peek();
  u8 dif = cpu->A - val;
  cpu->status.set_SZ(dif);
  cpu->status[Flag::C] = cpu->A >= val;
  cpu->tick(ticks + m.cross_page());
}

//! Compare with X
DECLARE_OP(CPX) {
  Mode m(cpu);
  u8 val = m.peek();
  u8 dif = cpu->X - val;
  cpu->status.set_SZ(dif);
  cpu->status[Flag::C] = cpu->X >= val;
  cpu->tick(ticks + m.cross_page());
}

//! Compare with Y
DECLARE_OP(CPY) {
  Mode m(cpu);
  u8 val = m.peek();
  u8 dif = cpu->Y - val;
  cpu->status.set_SZ(dif);
  cpu->status[Flag::C] = cpu->Y >= val;
  cpu->tick(ticks + m.cross_page());
}

//! Decrement memory
DECLARE_OP(DEC) {
  Mode m(cpu);
  u8 val = m.peek() - 1;
  m.poke(val);
  cpu->status.set_SZ(val);
  cpu->tick(ticks + m.cross_page());
}

//! Decrement X
DECLARE_OP(DEX) {
  Mode m(cpu);
  cpu->X--;
  cpu->status.set_SZ(cpu->X);
  cpu->tick(ticks + m.cross_page());
}

//! Decrement Y
DECLARE_OP(DEY) {
  Mode m(cpu);
  cpu->Y--;
  cpu->status.set_SZ(cpu->Y);
  cpu->tick(ticks + m.cross_page());
}

//! Bitwise xor
DECLARE_OP(EOR) {
  Mode m(cpu);
  cpu->A ^= m.peek();
  cpu->status.set_SZ(cpu->A);
  cpu->tick(ticks + m.cross_page());
}

//! Increment memory
DECLARE_OP(INC) {
  Mode m(cpu);
  u8 val = m.peek() + 1;
  m.poke(val);
  cpu->status.set_SZ(val);
  cpu->tick(ticks + m.cross_page());
}

//! Increment X register
DECLARE_OP(INX) {
  cpu->X++;
  cpu->status.set_SZ(cpu->X);
  cpu->tick(ticks);
}

//! Increment Y register
DECLARE_OP(INY) {
  cpu->Y++;
  cpu->status.set_SZ(cpu->Y);
  cpu->tick(ticks);
}

//! Jump
DECLARE_OP(JMP) {
  Mode m(cpu);
  cpu->IP = m.peekw();
  cpu->tick(ticks + m.cross_page());
}

//! Jump to subroutine
DECLARE_OP(JSR) {
  Mode m(cpu);
  u16 ip = cpu->IP - 1;
  u8 lo = ip % 256;
  u8 hi = u8(ip / 256);
  cpu->push(hi);
  cpu->push(lo);
  cpu->IP = m.addr;
  cpu->tick(ticks + m.cross_page());
}

//! Load A register
DECLARE_OP(LDA) {
  Mode m(cpu);
  cpu->A = m.peek();
  cpu->status.set_SZ(cpu->A);
  cpu->tick(ticks + m.cross_page());
}

//! Load X register
DECLARE_OP(LDX) {
  Mode m(cpu);
  cpu->X = m.peek();
  cpu->status.set_SZ(cpu->X);
  cpu->tick(ticks + m.cross_page());
}

//! Load Y register
DECLARE_OP(LDY) {
  Mode m(cpu);
  cpu->Y = m.peek();
  cpu->status.set_SZ(cpu->Y);
  cpu->tick(ticks + m.cross_page());
}

// Logical shift right
DECLARE_OP(LSR) {
  Mode m(cpu);
  u8 val = m.peek();
  cpu->status[Flag::C] = val & 0x01;
  val >>= 1;
  m.poke(val);
  cpu->status.set_SZ(val);
  cpu->tick(ticks + m.cross_page());
}

//! No operation
DECLARE_OP(NOP) {
  Mode m(cpu);
  cpu->tick(ticks + m.cross_page());
}

//! Bitwise or with A
DECLARE_OP(ORA) {
  Mode m(cpu);
  cpu->A |= m.peek();
  cpu->status.set_SZ(cpu->A);
  cpu->tick(ticks + m.cross_page());
}

//! Push accumulator
DECLARE_OP(PHA) {
  cpu->push(cpu->A);
  cpu->tick(ticks);
}

//! Push processor status
DECLARE_OP(PHP) {
  u8 val = cpu->status.pack();
  // special case
  val |= (1 << 4);
  cpu->push(val);
  cpu->tick(ticks);
}

//! Pull accumulator
DECLARE_OP(PLA) {
  cpu->A = cpu->pop();
  cpu->status.set_SZ(cpu->A);
  cpu->tick(ticks);
}

//! Pull processor status
DECLARE_OP(PLP) {
  cpu->status.unpack(cpu->pop());
  cpu->status[Flag::B] = false;
  cpu->status[Flag::U] = true;
  cpu->tick(ticks);
}

//! Rotate left
DECLARE_OP(ROL) {
  Mode m(cpu);
  u8 val = m.peek();
  u8 carry = (val & 0x80) >> 7;
  val <<= 1;
  val |= u8(cpu->status[Flag::C]);
  m.poke(val);
  cpu->status[Flag::C] = carry;
  cpu->status.set_SZ(val);
  cpu->tick(ticks + m.cross_page());
}

//! Rotate right
DECLARE_OP(ROR) {
  Mode m(cpu);
  u8 val = m.peek();
  u8 carry = val & 0x01;
  val >>= 1;
  val |= u8(cpu->status[Flag::C]) << 7;
  cpu->status[Flag::C] = carry;
  m.poke(val);
  cpu->status.set_SZ(val);
  cpu->tick(ticks + m.cross_page());
}

//! Return from interrupt
DECLARE_OP(RTI) {
  Mode m(cpu);
  std::cout << "RTI called @ " << cpu->sys->ppu.cycle << ", "
            << cpu->sys->ppu.scanline << std::endl;
  cpu->status.unpack(cpu->pop());
  cpu->status[Flag::U] = true;
  u8 lo = cpu->pop();
  u8 hi = cpu->pop();
  u16 addr = lo + 256 * u16(hi);
  cpu->IP = addr;
  cpu->tick(ticks + m.cross_page());
  cpu->interrupt_disabled = false;
}

//! Return from subroutine
DECLARE_OP(RTS) {
  Mode m(cpu);
  u8 lo = cpu->pop();
  u8 hi = cpu->pop();
  u16 addr = lo + 256 * u16(hi);
  cpu->IP = addr + 1;
  cpu->tick(ticks + m.cross_page());
}

//! Subtract with carry
DECLARE_OP(SBC) {
  Mode m(cpu);
  u16 c = cpu->status[Flag::C];
  u16 x = cpu->A;
  u16 y = m.peek() ^ 0xFF;
  u16 result = x + y + c;
  bool set_c = result > 0xFF;
  cpu->status[Flag::C] = set_c;

  i16 sx = x > 127 ? x - 256 : x;
  i16 sy = y > 127 ? y - 256 : y;
  i16 sr = sx + sy + c;
  cpu->status[Flag::V] = (sr > 127) || (sr < -128);

  cpu->A = u8(result);
  cpu->status.set_SZ(cpu->A);

  cpu->tick(ticks + m.cross_page());
}

//! Set carry flag
DECLARE_OP(SEC) {
  cpu->status[Flag::C] = true;
  cpu->tick(ticks);
}

//! Set decimal flag
DECLARE_OP(SED) {
  cpu->status[Flag::D] = true;
  cpu->tick(ticks);
}

//! Set interrupt disable flag
DECLARE_OP(SEI) {
  cpu->status[Flag::I] = true;
  cpu->tick(ticks);
}

//! Store accumulator
DECLARE_OP(STA) {
  Mode m(cpu);
  m.poke(cpu->A);
  // todo: special case!
  // cpu->tick(ticks + m.cross_page());
  cpu->tick(ticks);
}

//! Store X register
DECLARE_OP(STX) {
  Mode m(cpu);
  m.poke(cpu->X);
  // todo: special case!
  // cpu->tick(ticks + m.cross_page());
  cpu->tick(ticks);
}

//! Store Y register
DECLARE_OP(STY) {
  Mode m(cpu);
  m.poke(cpu->Y);
  // todo: special case!
  // cpu->tick(ticks + m.cross_page());
  cpu->tick(ticks);
}

//! Transfer A to X
DECLARE_OP(TAX) {
  TRANSFER(A, X);
  cpu->tick(ticks);
}

//! Transfer A to Y
DECLARE_OP(TAY) {
  TRANSFER(A, Y);
  cpu->tick(ticks);
}

//! Transfer stack pointer to X
DECLARE_OP(TSX) {
  TRANSFER(SP, X);
  // cpu->status[Flag::I] = true;
  cpu->tick(ticks);
}

//! Transfer X to A
DECLARE_OP(TXA) {
  TRANSFER(X, A);
  cpu->tick(ticks);
}

//! Transfer X to stack pointer
DECLARE_OP(TXS) {
  cpu->SP = cpu->X;
  cpu->tick(ticks);
}

//! Transfer Y to A
DECLARE_OP(TYA) {
  TRANSFER(Y, A);
  cpu->tick(ticks);
}

// Illegal operations follow below

//! Load A and X
DECLARE_OP(LAX) {
  Mode m(cpu);
  cpu->A = m.peek();
  cpu->X = cpu->A;
  cpu->status.set_SZ(cpu->A);
  cpu->tick(ticks + m.cross_page());
}

//! Dst = A&X
DECLARE_OP(SAX) {
  Mode m(cpu);
  m.poke(cpu->A & cpu->X);
  cpu->tick(ticks + m.cross_page());
}

//! Decrement and compare
DECLARE_OP(DCP) {
  Mode m(cpu);
  u8 a = cpu->A;
  u8 val = m.peek() - 1;
  m.poke(val);
  cpu->status[Flag::Z] = (a == val);
  cpu->status[Flag::S] = 0x80 & (a - val);
  cpu->status[Flag::C] = (a >= val);
  cpu->tick(ticks + m.cross_page());
}

//! Increment and subtract
DECLARE_OP(ISB) {
  Mode m(cpu);
  u16 c = cpu->status[Flag::C];
  u16 x = cpu->A;
  u8 mem = m.peek() + 1;
  m.poke(mem);
  u16 y = mem ^ 0xFF;
  u16 result = x + y + c;
  bool set_c = result > 0xFF;
  cpu->status[Flag::C] = set_c;

  i16 sx = x > 127 ? x - 256 : x;
  i16 sy = y > 127 ? y - 256 : y;
  i16 sr = sx + sy + c;
  cpu->status[Flag::V] = (sr > 127) || (sr < -128);

  cpu->A = u8(result);
  cpu->status.set_SZ(cpu->A);
  cpu->tick(ticks + m.cross_page());
}

//! ASL mem then or with A
DECLARE_OP(SLO) {
  Mode m(cpu);
  u8 val = m.peek();
  cpu->status[Flag::C] = val & 0x80;
  val <<= 1;
  m.poke(val);
  cpu->A |= val;
  cpu->status.set_SZ(cpu->A);
  cpu->tick(ticks + m.cross_page());
  // OR
}

//! ROL then AND with A
DECLARE_OP(RLA) {
  Mode m(cpu);
  u8 val = m.peek();
  u8 carry = (val & 0x80) >> 7;
  val <<= 1;
  val |= u8(cpu->status[Flag::C]);
  m.poke(val);
  cpu->status[Flag::C] = carry;
  cpu->A &= val;
  cpu->status.set_SZ(cpu->A);
  cpu->tick(ticks + m.cross_page());
}

//! LSR followedby EOR
DECLARE_OP(SRE) {
  Mode m(cpu);
  u8 val = m.peek();
  cpu->status[Flag::C] = val & 0x01;
  val >>= 1;
  m.poke(val);
  cpu->A ^= val;
  cpu->status.set_SZ(cpu->A);
  cpu->tick(ticks + m.cross_page());
}

//! ROR followed by ADC
DECLARE_OP(RRA) {
  Mode m(cpu);

  u8 val = m.peek();
  u8 carry = val & 0x01;
  val >>= 1;
  val |= u8(cpu->status[Flag::C]) << 7;
  cpu->status[Flag::C] = carry;
  m.poke(val);

  u16 c = cpu->status[Flag::C];
  u16 x = cpu->A;
  u16 y = val;
  u16 result = x + y + c;
  bool set_c = result > 0xFF;
  cpu->status[Flag::C] = set_c;

  i16 sx = x > 127 ? x - 256 : x;
  i16 sy = y > 127 ? y - 256 : y;
  i16 sr = sx + sy + c;
  cpu->status[Flag::V] = (sr > 127) || (sr < -128);

  cpu->A = u8(result);
  cpu->status.set_SZ(cpu->A);
  cpu->tick(ticks + m.cross_page());
}

#undef DECLARE_OP

CPU::CPU(System *sys) : sys(sys), instructions(256), instruction_length(256) {

  std::fill(instructions.begin(), instructions.end(), nullptr);

// build instruction table
#define DECLARE_INSTR(opcode, instr, mode, ticks)                              \
  instructions[opcode] = &_##instr<mode, ticks>;

#include "instr_list.hpp"
#undef DECLARE_INSTR
}

CPU::~CPU() {}

void CPU::tick(size_t count) {
  // todo: synchronize with PPU and APU
  while (count-- >= 1) {
    ++ticks;
    sys->ppu.tick();
    sys->ppu.tick();
    sys->ppu.tick();
  }
}

void CPU::exec() {
  if (interrupt_disabled == false) {
    if (nmi_requested) {
      std::cout << "NMI called @ " << sys->ppu.cycle << ", "
                << sys->ppu.scanline << std::endl;
      interrupt_disabled = true;
      nmi_requested = false;
      u8 lo = u8(IP);
      u8 hi = u8(IP >> 8);
      push(hi);
      push(lo);
      push(status.pack());
      IP = sys->peekw(CPU::nmi_vector);
      tick(7);
      return;
    }
    if (interrupt_requested && !status[Flag::I]) {
      interrupt_disabled = true;
      interrupt_requested = false;
      u8 lo = u8(IP);
      u8 hi = u8(IP >> 8);
      push(hi);
      push(lo);
      push(status.pack());
      IP = sys->peekw(CPU::irq_vector);
      tick(7);
      return;
    }
  }

  u8 opcode;
  // u16 old_IP = IP;
  opcode = sys->peek(IP++);
  // std::cout << std::hex << old_IP << ": " << mnemonic(opcode) << std::endl;

  auto op = instructions[opcode];

  if (op == nullptr) {
    std::stringstream msg;
    msg << "illegal opcode encountered: ";
    msg << std::hex << u32(opcode);
    throw std::runtime_error(msg.str());
  }

  instructions[opcode](this);
}

std::string CPU::disassemble() {
#if 0
  std::stringstream s;
  const char* mnemonic;
  AddrType mode;


  u16 opcode, lo, hi;
  opcode = sys->peek(IP++);
  u8 length = instruction_length[opcode];

  s << std::uppercase << std::hex << std::setfill('0') << std::setw(4) << IP;
  s << std::setw(2) << "  " << opcode << " ";

  if (length > 1) {
    lo = sys->peek(IP+1);
    s << lo << " ";
  } else {
    s << "   ";
  }

  if (length > 2) {
    hi = sys->peek(IP+2);
    s << hi << "  ";
  } else {
    s << "    ";
  }

#define DECLARE_INSTR(opcode, name, m, ticks)                                  \
  case opcode:                                                                 \
    mnemonic = #name;                                                          \
    mode = AddrType::m;                                                        \
    break;

  switch(opcode) {
#include "instr_list.hpp"
    default: s << "???"; return s.str();
  }
#undef DECLARE_INSTR

  s << mnemonic << " ";
  s << std::setw(2);

  switch (mode) {
    case AddrType::ACC: s << "A"; break;
    case AddrType::IMM: s << "#$" << lo; break;
    case AddrType::IMP: break;
    case AddrType::REL: s << "$" << lo; break;
    case AddrType::ABS: s << "$" << hi << lo; break;
    case AddrType::ABSX: s << "$" << hi << lo << ", X"; break;
    case AddrType::ABSY: s << "$" << hi << lo << ", Y"; break;
    case AddrType::ZP: s << "$" << lo; break;
    case AddrType::ZPX: s << "$" << lo << ", X"; break;
    case AddrType::ZPY: s << "$" << lo << ", Y"; break;
    case AddrType::IND: s << "($" << hi << lo << ")"; break;
    case AddrType::INDX: s << "($" << lo << ", X)"; break;
    case AddrType::INDY: s << "($" << lo << "), Y"; break;
  }

  return s.str();
#else
  return "";
#endif
}

void CPU::push(u8 val) {
  u16 addr = 0x0100 + u16(SP--);
  sys->poke(addr, val);
}

u8 CPU::pop() {
  u16 addr = 0x0100 + u8(++SP);
  return sys->peek(addr);
}

void CPU::reset() {
  A = X = Y = 0;
  IP = 0;
  SP = 0xFF;

  interrupt_requested = false;
  interrupt_disabled = false;
  nmi_requested = false;

  u16 addr = sys->peekw(reset_vector);
  SP = 0xFD;
  IP = addr;
  status.unpack(0);
  status[Flag::I] = true;
  status[Flag::U] = true;
}

std::string CPU::mnemonic(u8 opcode) {
  std::stringstream s;

#define DECLARE_INSTR(op, name, m, ticks)                                      \
  case op:                                                                     \
    s << #name;                                                                \
    break;

  switch (opcode) {
#include "instr_list.hpp"
  default:
    s << "???";
  }
#undef DECLARE_INSTR

  return s.str();
}
