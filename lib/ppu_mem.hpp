#ifndef NESEMU_PPU_MEM_HPP_INCLUDED
#define NESEMU_PPU_MEM_HPP_INCLUDED

#include <array>
#include <memory>

#include "types.hpp"

namespace nesemu {
class PPUMemory {
private:
  std::array<u8, 0x400 * 4> nametable_buffer;
  Mirroring mirror_mode;

public:
  u8 *nametables[4];
  Palette palette;
  MemBank pattern[2];

  PPUMemory();

  u8 &operator[](u16 addr);

  void set_mirror_mode(Mirroring mode);
};
} // namespace nesemu

#endif
