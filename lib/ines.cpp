#include "ines.hpp"

#include "mapper/nrom.hpp"

#include <stdexcept>

using namespace nesemu;

static_assert(sizeof(NesHeader) == header_size, "iNes header size");

void NesRom::open(std::ifstream &s) {
  s.seekg(0, std::ios_base::beg);
  s.read(reinterpret_cast<char *>(&header), sizeof(NesHeader));

  // magic code
  bool valid = true;
  valid &= header.nes[0] == 'N';
  valid &= header.nes[1] == 'E';
  valid &= header.nes[2] == 'S';
  valid &= header.nes[3] == 0x1A;

  if (!valid) {
    throw std::runtime_error("NesRom: invalid header");
  }

  // get mapper id
  mapper_id = (header.flags6 >> 4) | (header.flags7 & 0xF0);

  // set up mapper
  switch (mapper_id) {
  case 0:
    mapper.reset(new nesemu::mapper::NROM);
    break;
  default:
    throw std::runtime_error("NesRom(): unsupported mapper " +
                             std::to_string(mapper_id));
  }

  // allocate rom and ram banks
  mapper->prg_rom.resize(2 * header.n_prg_rom * bank_size);
  mapper->chr_rom.resize(header.n_chr_rom * bank_size);
  mapper->prg_ram.resize(header.n_prg_ram * bank_size);

  // rom has trainer
  bool trainer = header.flags6 & (1 << 2);

  mirror_mode =
      (header.flags6 & 1) ? Mirroring::VERTICAL : Mirroring::HORIZONTAL;

  const std::streamoff prg_start = header_size + (trainer ? trainer_size : 0);
  const std::streamoff chr_start = prg_start + mapper->prg_rom.size();

  // read prg
  s.seekg(0, std::ios_base::beg);
  s.seekg(prg_start);
  s.read(reinterpret_cast<char *>(&mapper->prg_rom[0]), mapper->prg_rom.size());

  // read chr
  s.seekg(0, std::ios_base::beg);
  s.seekg(chr_start);
  s.read(reinterpret_cast<char *>(&mapper->chr_rom[0]), mapper->chr_rom.size());
}
