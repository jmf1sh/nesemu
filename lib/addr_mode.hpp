#ifndef NESEMU_ADDR_MODE_HPP_INCLUDED
#define NESEMU_ADDR_MODE_HPP_INCLUDED

#include "cpu.hpp"
#include "system.hpp"
#include "types.hpp"
#include <iostream>
#include <string>

namespace nesemu {
struct CPU;

struct AddrBase {
  virtual u16 fetch(CPU *cpu) const = 0;
  virtual std::string print() const = 0;
};

struct AddrMode {
  CPU *cpu;
  u16 addr;

  u8 peek() { return cpu->sys->peek(addr); }
  u16 peekw() { return cpu->sys->peekw(addr); }
  void poke(u8 val) { cpu->sys->poke(addr, val); }
  bool cross_page() { return false; }
};

struct OopsAddrMode : public AddrMode {
  u16 base_addr;
  bool cross_page() { return (addr & 0xFF00) != (base_addr & 0xFF00); }
};

struct ACC : public AddrMode {
  ACC(CPU *cpu_) { cpu = cpu_; }
  u8 peek() { return cpu->A; }
  void poke(u8 val) { cpu->A = val; }
};

struct IMM : public AddrMode {
  IMM(CPU *cpu_) {
    cpu = cpu_;
    addr = cpu->IP++;
  }
};

struct IMP : public AddrMode {
  IMP(CPU *cpu_) { cpu = cpu_; }
};

struct REL : public OopsAddrMode {
  REL(CPU *cpu_) {
    cpu = cpu_;
    base_addr = cpu->IP + 1;
    addr = base_addr + i8(cpu->sys->peek(cpu->IP));
  }
};

struct ABS : public AddrMode {
  ABS(CPU *cpu_) {
    cpu = cpu_;
    addr = cpu->sys->peekw(cpu->IP);
    cpu->IP += 2;
  }
};

struct ABSX : public OopsAddrMode {
  ABSX(CPU *cpu_) {
    cpu = cpu_;
    base_addr = cpu->sys->peekw(cpu->IP);
    addr = base_addr + cpu->X;
    cpu->IP += 2;
  }
};

struct ABSY : public OopsAddrMode {
  ABSY(CPU *cpu_) {
    cpu = cpu_;
    base_addr = cpu->sys->peekw(cpu->IP);
    addr = base_addr + cpu->Y;
    cpu->IP += 2;
  }
};

struct ZP : public AddrMode {
  ZP(CPU *cpu_) {
    cpu = cpu_;
    addr = cpu->sys->peek(cpu->IP++);
  }
};

struct ZPX : public AddrMode {
  ZPX(CPU *cpu_) {
    cpu = cpu_;
    addr = cpu->sys->peek(cpu->IP++) + cpu->X;
    addr %= 256;
  }
};

struct ZPY : public AddrMode {
  ZPY(CPU *cpu_) {
    cpu = cpu_;
    addr = cpu->sys->peek(cpu->IP++) + cpu->Y;
    addr %= 256;
  }
};

struct IND : public AddrMode {
  IND(CPU *cpu_) {
    cpu = cpu_;
    addr = cpu->sys->peekw(cpu->IP);
    cpu->IP += 2;
  }

  u16 peekw() {
    u16 page = addr & 0xFF00;
    u16 off = addr & 0x00FF;

    u16 lo = cpu->sys->peek(addr);
    u16 hi = cpu->sys->peek(page | ((off + 1) & 0xFF));

    return 256 * hi + lo;

    // return cpu->sys->peekw(256*hi+lo);
  }
};

struct INDX : public AddrMode {
  INDX(CPU *cpu_) {
    cpu = cpu_;
    u16 a = cpu->sys->peek(cpu->IP++);
    addr = cpu->sys->peek((a + cpu->X) % 256) +
           256 * u16(cpu->sys->peek((a + cpu->X + 1) % 256));
  }
};

struct INDY : public OopsAddrMode {
  INDY(CPU *cpu_) {
    cpu = cpu_;
    u16 a = cpu->sys->peek(cpu->IP++);
    base_addr = cpu->sys->peek(a) + 256 * u16(cpu->sys->peek((a + 1) % 256));
    addr = base_addr + cpu->Y;
  }
};
} // namespace nesemu

#endif
