#include "tile.hpp"

using namespace nesemu;

Tile::Tile(const u8 bytes[16]) {
  for (int row = 0; row < 8; ++row) {
    u8 lo = bytes[row];
    u8 hi = bytes[row + 8];

    for (int col = 0; col < 8; ++col) {
      u8 px = 0;
      px |= (lo >> (7 - col)) & 0x01;
      px |= ((hi >> (7 - col)) & 0x01) << 1;
      pixels[row * 8 + col] = px;
    }
  }
}

Tile::Tile(MemBank &bank, u8 index) {
  u16 offset = 16 * u16(index);

  for (int row = 0; row < 8; ++row) {
    u8 lo = bank[offset + row];
    u8 hi = bank[offset + row + 8];

    for (int col = 0; col < 8; ++col) {
      u8 px = 0;
      px |= (lo >> (7 - col)) & 0x01;
      px |= ((hi >> (7 - col)) & 0x01) << 1;
      pixels[row * 8 + col] = px;
    }
  }
}
