#ifndef NESEMU_SYSTEM_HPP_INCLUDED
#define NESEMU_SYSTEM_HPP_INCLUDED

#include "apu.hpp"
#include "cpu.hpp"
#include "ines.hpp"
#include "joypad.hpp"
#include "ppu.hpp"
#include "types.hpp"
#include <vector>

namespace nesemu {

constexpr size_t ram_size = 2 * 1024;

struct System {
  std::vector<u8> ram;
  NesRom rom;
  APU apu;
  PPU ppu;
  CPU cpu;
  JoyPad joypad;

  System();

  void reset();

  u8 peek(u16 addr);
  u16 peekw(u16 addr);
  void poke(u16 addr, u8 val);

  //! Step through a complete frame
  void step_frame();
};
} // namespace nesemu

#endif
