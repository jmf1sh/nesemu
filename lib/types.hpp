#ifndef NESEMU_TYPES_HPP_INCLUDED
#define NESEMU_TYPES_HPP_INCLUDED

#include <array>
#include <cstdint>

namespace nesemu {
using i8 = std::int8_t;
using u8 = uint8_t;
using i16 = int16_t;
using u16 = uint16_t;
using i32 = int32_t;
using u32 = uint32_t;

using MemBank = std::array<u8, 0x1000>;
using NameTable = std::array<u8, 0x0400>;
using Palette = std::array<u8, 0x0020>;

enum class Mirroring { HORIZONTAL, VERTICAL, SINGLE, QUAD };
enum class Buttons : u8 {
  A = 0,
  B = 1,
  SELECT = 2,
  START = 3,
  UP = 4,
  DOWN = 5,
  LEFT = 6,
  RIGHT = 7
};

using JoyState = std::array<bool, 8>;

} // namespace nesemu

#endif
