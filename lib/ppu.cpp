#include "ppu.hpp"
#include "system.hpp"

#include <cassert>
#include <iostream>
#include <stdexcept>

using namespace nesemu;

// ppustatus flags
static const u8 VBLANK_MASK = 0b10000000;
static const u8 SPRITE0_MASK = 0b01000000;
static const u8 BACKGROUND_ENABLE_MASK = 0b00001000;
static const u8 SPRITE_ENABLE_MASK = 0b00010000;

// ppuctrl flags
static const u8 BASE_NAMETABLE_MASK = 0b00000011;
static const u8 VRAM_INCREMENT_MASK = 0b00000100;
static const u8 SPRITE_PATTERN_MASK = 0b00001000;
static const u8 BACKGROUND_PATTERN_MASK = 0b00010000;
static const u8 SPRITE_SIZE_MASK = (1 << 5);
static const u8 NMI_MASK = (1 << 7);

// masks for internal registers
static const u16 INTERNAL_NAMETABLE_MASK = 0b11 << 10;
static const u16 INTERNAL_COARSE_X_MASK = 0b11111;
static const u16 INTERNAL_COARSE_Y_MASK = 0b11111 << 5;
static const u16 INTERNAL_FINE_Y_MASK = 0b111 << 12;
static const u16 INTERNAL_HORIZONTAL_MASK = 0b10000011111;
static const u16 INTERNAL_VERTICAL_MASK = 0b111101111100000;

// OAM attribute
static const u8 OAM_ATTRIBUTE_HORIZONTAL = 0b01000000;
static const u8 OAM_ATTRIBUTE_VERTICAL = 0b10000000;

static const i32 warmup_cycles = 29658;

PPU::PPU(System *s) {
  sys = s;

  fb[0].resize(256 * 240);
  fb[1].resize(256 * 240);

  scanline_oam.reserve(8);
  scanline_idx.reserve(8);
  scanline_pt.reserve(8);

  reset();
}

void PPU::reset() {
  // Information on start-up state
  // https://wiki.nesdev.com/w/index.php/PPU_power_up_state
  ppuctrl = 0;
  ppumask = 0;
  ppustatus = 0;
  oamaddr = 0;
  oamdma = 0;
  // todo: 0x2005, 0x2006 latch
  odd_frame = false;

  // internal registers
  v = t = x = 0;
  w = false;

  scanline = 0;
  cycle = 0;

  warm = false;

  std::memset(oam, 0, sizeof(oam));
}

void PPU::tick() {
  // todo: rendering!
  // const bool render = rendering();

  if (scanline >= 0 && scanline < 240) {
    visible_scanline();
  } else if (scanline == 240) {
    postrender_scanline();
  } else if (scanline > 240 && scanline <= 260) {
    vblank_scanline();
  } else if (scanline == 261) {
    prerender_scanline();
  }

  if (++cycle > 340) {
    cycle = 0;
    if (++scanline > 261) {
      scanline = 0;
      odd_frame = !odd_frame;
      std::cout << "DEBUG ODD FRAME " << odd_frame << std::endl;
    }
  }
}

void PPU::prerender_scanline() {
  const bool render = rendering();

  // ????
  if (cycle == 0) {
    ppustatus &= ~VBLANK_MASK;
    ppustatus &= ~SPRITE0_MASK;
    // odd frame cycle skip
    if (render && odd_frame) {
      ++cycle;
    }
  }

  if (cycle == 304) {
    if (render) {
      // copy vertical bits from t to v
      v = (v & ~INTERNAL_VERTICAL_MASK) | (t & INTERNAL_VERTICAL_MASK);
    }
  }
}

void PPU::visible_scanline() {
  const bool render = rendering();

  if (cycle == 0) {
    prepare_scanline();
  } else if ((cycle <= 256) && render) {
    render_pixel();
    increment_x();
  }

  if ((cycle == 256) && render) {
    increment_y();
  }

  if ((cycle == 257) && render) {
    // copy horizontal bits from t to v
    v = (v & ~INTERNAL_HORIZONTAL_MASK) | (t & INTERNAL_HORIZONTAL_MASK);
    x = tx;
  }
}

void PPU::postrender_scanline() {
  // double buffering

  if (cycle == 0) {
    std::swap(fb[0], fb[1]);
    std::fill(fb[1].begin(), fb[1].end(), 0);
  }
}

void PPU::vblank_scanline() {
  if (scanline == 241 && cycle == 1) {
    ppustatus |= VBLANK_MASK;

    if (ppuctrl & NMI_MASK) {
      if (!sys->cpu.interrupt_disabled) {
        sys->cpu.nmi_requested = true;
      }
    }
  }
}

void PPU::poke(u16 addr, u8 val) {
  u8 inc = (ppuctrl & VRAM_INCREMENT_MASK) ? 32 : 1;

  if (!warm) {
    warm = (scanline + 1) * 341 * 3 >= warmup_cycles;
  }

  switch (addr) {
  case 0x2000:
    if (warm) {
      ppuctrl = val;
      // update base nametable
      t = (t & ~INTERNAL_NAMETABLE_MASK) |
          ((ppuctrl & BASE_NAMETABLE_MASK) << 10);
    }
    break;
  case 0x2001:
    if (warm) {
      ppumask = val;
    }
    break;
  case 0x2002:
    ppustatus = val;
    break;
  case 0x2003:
    oamaddr = val;
    break;
  case 0x2004:
    oam[oamaddr++] = val;
    break;
  case 0x2005:
    if (warm) {
      // debug
      if (scanline <= 240) {
        std::cout << "#######################################" << std::endl;
        std::cout << "ppuscroll write during frame @ " << cycle << ", "
                  << scanline << std::endl;
      }

      // ppuscroll
      // first write
      if (!w) {
        t = (t & ~INTERNAL_COARSE_X_MASK) |
            ((val >> 3) & INTERNAL_COARSE_X_MASK);
        tx = val & 0b111;
        std::cout << "ppu scroll x " << u16(val) << std::endl;
      } else {
        t = (t & ~INTERNAL_COARSE_Y_MASK) |
            ((val << 2) & INTERNAL_COARSE_Y_MASK);
        t = (t & ~INTERNAL_FINE_Y_MASK) | ((val & 0b111) << 12);
        std::cout << "ppu scroll y " << u16(val) << std::endl;
        u16 fine_y = (t & INTERNAL_FINE_Y_MASK) >> 12 |
                     (t & INTERNAL_COARSE_Y_MASK) >> 2;
        std::cout << "fine y " << fine_y << std::endl;
      }
      w = !w;
    }
    break;
  case 0x2006:
    if (warm) {
      // debug
      if (scanline <= 240) {
        std::cout << "#######################################" << std::endl;
        std::cout << "ppuaddr write during frame @ " << cycle << ", "
                  << scanline << std::endl;
      }

      // ppuaddr
      if (!w) {
        // t = (t & 0x00FF) | ((val & 0b111111) << 8);
        t = (t & 0x00FF) | ((val & 0b11111111) << 8);
      } else {
        t = (t & 0xFF00) | val;
        v = t;
      }
      w = !w;
    }
    break;
  case 0x2007:
    // ppudata
    // todo: this needs to be handled by cartridge mapper
    if (v >= 0x2000) {
      vram[v] = val;
    }
    v += inc;
    std::cout << "ppudata @ " << cycle << ", " << scanline << std::endl;
    break;

  case 0x4014:
    oamdma = val;
    dma();
    break;
  }

  latch = val;
}

u8 PPU::peek(u16 addr) {
  // used for ppu vram read delay
  u8 old_latch = latch;
  // vram increment
  u8 inc = (ppuctrl & VRAM_INCREMENT_MASK) ? 32 : 1;

  switch (addr) {

  case 0x2002:
    // ppustatus
    w = false;
    latch = ppustatus;
    // clear vblank bit
    ppustatus &= ~VBLANK_MASK;
    break;

  case 0x2004:
    // oamdata
    latch = oam[oamaddr];
    break;

  case 0x2007:
    // ppudata
    latch = vram[v];
    v += inc;

    // palette reads are not delayed
    if (addr >= 0x3F00) {
      old_latch = latch;
    }

    break;
  }
  return old_latch;
}

int PPU::cyc() const { return cycle; }

void PPU::dma() {
  for (auto i = 0; i < 256; ++i) {
    u16 addr = u16(oamdma) * 256 + i;
    oam[oamaddr++] = sys->peek(addr);
  }

  // ppu active while CPU sleeps for 513 cycles
  for (auto i = 0; i < 513 * 3; ++i) {
    tick();
  }
}

// https://wiki.nesdev.com/w/index.php/PPU_sprite_evaluation
void PPU::prepare_scanline() {
  scanline_oam.clear();
  scanline_idx.clear();
  scanline_pt.clear();

  // find up to 8 sprites on current scanline
  for (auto oam_idx = 0; oam_idx < 64; ++oam_idx) {
    if (scanline_oam.size() >= 8) {
      break;
    }

    auto spr = reinterpret_cast<const OAM *>(&oam[sizeof(OAM) * oam_idx]);
    bool hit = (spr->y <= scanline) && (spr->y + 8 > scanline);

    if (hit) {
      scanline_oam.push_back(*spr);
      scanline_idx.push_back(oam_idx);
      scanline_pt.push_back((ppuctrl & SPRITE_PATTERN_MASK) ? 1 : 0);
    }

    // todo: sprite overflow!
  }
}

void PPU::render_pixel() {
  if (cycle == 0 || cycle > 256) {
    return;
  }

  u8 bg_px = 0;

  bool background_enable = ppumask & BACKGROUND_ENABLE_MASK;
  bool sprite_enable = ppumask & SPRITE_ENABLE_MASK;

  auto dot = cycle - 1;

  const u8 scroll_x = ((v & INTERNAL_COARSE_X_MASK) << 3) | x;
  const u8 scroll_y =
      (((v & INTERNAL_FINE_Y_MASK) >> 12) | (v & INTERNAL_COARSE_Y_MASK) >> 2);

  if (background_enable) {
    u16 sx = u16(scroll_x);
    u16 sy = u16(scroll_y);

    /*
    std::cout << std::dec;
    std::cout << u16(dot) << ", " << scanline << ": " << sx << ", " << sy;
    std::cout << " x = " << u16(x);
    std::cout << " v = " << std::hex << v << std::endl;
    */

    u8 bg_nametable_index = (v & INTERNAL_NAMETABLE_MASK) >> 10;
    // u8 bg_nametable_index = ppuctrl & 0b11;

    /*

    if (sx > 256) {
      bg_nametable_index += 1;
    }
    if (sy > 240) {
      bg_nametable_index += 2;
    }
    */

    bg_nametable_index %= 4;

    u8 *bg_nametable = vram.nametables[bg_nametable_index];

    u8 bg_x = u8(sx % 256);
    u8 bg_y = u8(sy % 240);

    // background from nametable 0
    u16 tile_x = bg_x / 8;
    u16 tile_y = bg_y / 8;
    u8 bg_pattern = (ppuctrl & BACKGROUND_PATTERN_MASK) ? 1 : 0;
    u8 bg_sprite = bg_nametable[32 * tile_y + tile_x];

    u16 attr_x = tile_x / 4;
    u16 attr_y = tile_y / 4;

    u8 attr = bg_nametable[0x3C0 + 8 * attr_y + attr_x];

    bool top = (tile_y % 4) <= 1;
    bool left = (tile_x % 4) <= 1;

    u8 bg_pal;

    if (top) {
      bg_pal = left ? attr : attr >> 2;
    } else {
      bg_pal = left ? attr >> 4 : attr >> 6;
    }

    bg_pal &= 0b11;

    // Tile bg_tile(vram.pattern[bg_pattern], bg_sprite);
    // u8 bg_px = bg_tile.pixels[8*(scanline%8) + (x%8)];

    bg_px = read_tile(bg_pattern, bg_sprite, sx % 8, sy % 8);

    // u16 bg_pal_addr = 0x3F00 + 4*bg_pal + bg_px;
    u16 bg_pal_addr = 4 * bg_pal + bg_px;

    if (bg_px > 0) {
      fb[1][256 * scanline + dot] = (63 & vram.palette[bg_pal_addr]);
    } else {
      fb[1][256 * scanline + dot] = (63 & vram.palette[0]);
    }
  } else {
    fb[1][256 * scanline + dot] = (63 & vram.palette[0]);
  }

  if (!sprite_enable) {
    return;
  }

  for (unsigned i = 0; i < scanline_oam.size(); ++i) {
    const auto &spr = scanline_oam[i];

    if (dot < spr.x || dot >= spr.x + 8) {
      continue;
    }

    auto dx = dot - spr.x;
    auto dy = scanline - spr.y;
    const u8 pal = (spr.attr & 0x3);

    if (spr.attr & OAM_ATTRIBUTE_HORIZONTAL) {
      dx = 7 - dx;
    }
    if (spr.attr & OAM_ATTRIBUTE_VERTICAL) {
      dy = 7 - dy;
    }

    // https://wiki.nesdev.com/w/index.php/PPU_palettes
    u8 px = read_tile(scanline_pt[i], spr.idx, dx, dy);

    if (px == 0) {
      continue;
    }

    // sprite 0 hit
    if (bg_px > 0 && scanline_idx[i] == 0) {
      ppustatus |= SPRITE0_MASK;
    }

    // u16 pal_addr = 0x3F10 + 4*pal + px;
    u16 pal_addr = 4 * pal + px;

    assert(px < 4);
    assert(pal < 4);
    // read palette value; top two bits ignored!
    u8 index = (vram.palette[0x10 + pal_addr] & 63);

    // todo: look up color in global palette!!
    if (px != 0) {
      fb[1][256 * scanline + dot] = index;
      // sprite priority
      break;
    }
  }
}

u8 PPU::read_tile(u8 pattern_index, u8 index, u8 x, u8 y) {
  u16 offset = 16 * u16(index);
  const MemBank &bank = vram.pattern[pattern_index % 2];
  u8 lo = bank[offset + y];
  u8 hi = bank[offset + y + 8];
  u8 px = (lo >> (7 - x)) & 0x01;
  px |= ((hi >> (7 - x)) & 0x01) << 1;
  return px;
}

void PPU::increment_x() {
  if (++x == 8) {
    x = 0;
    if ((v & INTERNAL_COARSE_X_MASK) == 31) {
      v &= ~INTERNAL_COARSE_X_MASK; // coarse X = 0
      v ^= 1 << 10;                 // switch horizontal nametable
    } else {
      v += 1; // increment coarse X
    }
  }
}

void PPU::increment_y() {
  if ((v & 0x7000) != 0x7000) {
    v += 0x1000;
    return;
  }

  v &= ~0x7000;
  int y = (v & 0x03E0) >> 5;
  if (y == 29) {
    y = 0;
    v ^= 0x0800;
  } else if (y == 31) {
    y = 0;
  } else {
    y += 1;
  }
  v = (v & ~0x03E0) | (y << 5);
}

bool PPU::rendering() const {
  return (ppumask & BACKGROUND_ENABLE_MASK) | (ppumask & SPRITE_ENABLE_MASK);
}
