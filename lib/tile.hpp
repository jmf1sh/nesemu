#ifndef NESEMU_TILE_HPP_INCLUDED
#define NESEMU_TILE_HPP_INCLUDED

#include "types.hpp"

namespace nesemu {
struct Tile {
  u8 pixels[8 * 8];

  Tile(const u8 *bytes);
  Tile(MemBank &bank, u8 index);
};
} // namespace nesemu

#endif
