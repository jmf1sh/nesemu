#ifndef NESEMU_JOYPAD_HPP_INCLUDED
#define NESEMU_JOYPAD_HPP_INCLUDED

#include "types.hpp"

namespace nesemu {
struct JoyPad {
  bool strobe;
  u8 index;
  JoyState state[2];

  JoyPad();

  u8 peek(u16 addr);
  void poke(u16 addr, u8 val);
};
} // namespace nesemu

#endif
