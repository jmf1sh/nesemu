#ifndef NESEMU_MAPPER_NROM_HPP_INCLUDED
#define NESEMU_MAPPER_NROM_HPP_INCLUDED

#include "../ines.hpp"
#include "../types.hpp"

namespace nesemu {
namespace mapper {
struct NROM : public Mapper {
  NROM();
  ~NROM();
  u8 peek(u16 addr) override;
  void poke(u16 addr, u8 val) override;

private:
  template <bool write> u8 access(u16 addr, u8 val = 0);
};
} // namespace mapper
} // namespace nesemu

#endif
