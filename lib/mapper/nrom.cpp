#include "nrom.hpp"

using namespace nesemu;
using namespace nesemu::mapper;

NROM::NROM() {}

NROM::~NROM() {}

template <bool write> u8 NROM::access(u16 addr, u8 val) {
  if (addr < chr_rom.size()) {
    return chr_rom[addr];
  }

  // program ROM
  if (addr >= 0x8000) {
    // mirror if necessary
    addr -= 0x8000;
    addr %= prg_rom.size();
  }

  if (write) {
    prg_rom[addr] = val;
    return val;
  } else {
    return prg_rom[addr];
  }
}

u8 NROM::peek(u16 addr) { return access<false>(addr); }

void NROM::poke(u16 addr, u8 val) { access<true>(addr, val); }
