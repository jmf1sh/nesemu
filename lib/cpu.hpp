#ifndef NESEMU_CPU_HPP_INCLUDED
#define NESEMU_CPU_HPP_INCLUDED

#include "status.hpp"
#include "types.hpp"

#include <string>
#include <vector>

namespace nesemu {
struct System;

struct CPU {
  //! NMI vector location
  static constexpr size_t nmi_vector = 0xFFFA;
  //! Reset vector location
  static constexpr size_t reset_vector = 0xFFFC;
  //! IRQ vector location
  static constexpr size_t irq_vector = 0xFFFE;
  //! 8-bit registers
  u8 A, X, Y, SP;
  //! Status register
  Status status;
  //! 16-bit registers
  u16 IP;
  //! clock tick counter
  u32 ticks;
  //! system to which cpu belongs
  System *sys;

  using Op = void (*)(CPU *);
  std::vector<Op> instructions;
  std::vector<u8> instruction_length;

  bool nmi_requested;
  bool interrupt_requested;
  bool interrupt_disabled;

  CPU(System *sys);
  ~CPU();

  //! Push onto stack
  void push(u8 val);
  //! Pop from stack
  u8 pop();
  //! Reset the cpu
  void reset();

  //! Increment the cycle count, synchronize with PPU and APU
  void tick(size_t count = 1);
  //! Execute the current instruction
  void exec();
  //! Pretty print the current instruction
  std::string disassemble();

  //! Mnemonic for opcode
  std::string mnemonic(u8 opcode);
};
} // namespace nesemu

#endif
