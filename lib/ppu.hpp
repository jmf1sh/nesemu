#ifndef NESEMU_PPU_HPP_INCLUDED
#define NESEMU_PPU_HPP_INCLUDED

#include <cstring>
#include <vector>

#include "ppu_mem.hpp"
#include "types.hpp"

namespace nesemu {
struct System;

//! OAM structure
//! See https://wiki.nesdev.com/w/index.php/PPU_OAM
struct OAM {
  u8 y;
  u8 idx;
  u8 attr;
  u8 x;
};

struct PPU {
  //! Parent system
  System *sys;

  //! True if ppu already warmed up
  bool warm;

  //! Internal address registers
  u16 v;
  //! Internal temporary address register
  u16 t;
  //! Fine x scroll
  u8 x;
  //! Fine x scroll temporary
  u8 tx;
  //! Second write flag
  bool w;

  //! PPU registers. See https://wiki.nesdev.com/w/index.php/PPU_registers
  u8 ppuctrl;
  u8 ppumask;
  u8 ppustatus;
  //! OAM address to write to
  u8 oamaddr;
  //! Address to read from during oamdma
  u8 oamdma;

  //! Latch for reading write-only registers
  u8 latch;

  //! Frame buffers (used for double buffering)
  std::vector<u8> fb[2];

  //! Current scanline
  int scanline;

  //! Current cycle of the scaneline
  int cycle;

  //! Object attribute memory
  u8 oam[256];

  //! OAM of sprites to render on current scanline
  std::vector<OAM> scanline_oam;
  //! Indices of sprites to render on current scanline
  std::vector<u8> scanline_idx;
  //! Indices of the pattern tables used for sprites
  std::vector<u8> scanline_pt;

  //! Internal ram
  PPUMemory vram;

  //! Currently on odd frame
  bool odd_frame;

  PPU(System *);

  void tick();
  void poke(u16 addr, u8 val);
  // todo: should this be allowed??
  u8 peek(u16 addr);
  int cyc() const;

  u8 read_tile(u8 pattern_index, u8 index, u8 x, u8 y);

  void dma();

  void reset();

private:
  void prerender_scanline();
  void visible_scanline();
  void postrender_scanline();
  void vblank_scanline();

private:
  void prepare_scanline();
  void render_pixel();
  void increment_x();
  void increment_y();
  bool rendering() const;
};
} // namespace nesemu

#endif
