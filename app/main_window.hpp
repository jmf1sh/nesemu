#ifndef MAIN_WINDOW_HPP_INCLUDED
#define MAIN_WINDOW_HPP_INCLUDED

#include <QElapsedTimer>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMainWindow>
#include <QTimer>

#include "input_filter.hpp"
#include "system.hpp"

class MainWindow : public QMainWindow {
  Q_OBJECT
public:
  MainWindow();

private slots:
  void open();
  void about();

private:
  void createActions();
  void createStatusBar();
  void setScale(double scale);

  void step_frame();

private:
  qint64 elapsed;
  QElapsedTimer elapsed_timer;
  QTimer *timer;
  QGraphicsView *view;
  QGraphicsScene *scene;
  QImage *image;
  QGraphicsPixmapItem *pixmap;
  nesemu::System sys;
  std::vector<nesemu::u32> frame_data;
  InputFilter *filter;
};

#endif
