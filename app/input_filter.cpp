#include "input_filter.hpp"

#include <iostream>

#include <QKeyEvent>

InputFilter::InputFilter(QObject *parent) : QObject(parent) {
  //  left = right = up = down = false;
  // b = a = select = start = false;
  std::fill(state.begin(), state.end(), 0);
}

bool InputFilter::eventFilter(QObject *obj, QEvent *event) {

  auto type = event->type();

  if (type != QEvent::KeyPress && type != QEvent::KeyRelease) {
    return QObject::eventFilter(obj, event);
  }

  auto key_event = static_cast<QKeyEvent *>(event);

  auto setk = [type, key_event](bool *k) {
    if (type == QEvent::KeyPress)
      *k = true;
    else if (type == QEvent::KeyRelease)
      *k = false;
  };

  switch (key_event->key()) {
  case Qt::Key_S:
    setk(&state[0]);
    break;
  case Qt::Key_A:
    setk(&state[1]);
    break;
  case Qt::Key_Q:
    setk(&state[2]);
    break;
  case Qt::Key_W:
    setk(&state[3]);
    break;
  case Qt::Key_Up:
    setk(&state[4]);
    break;
  case Qt::Key_Down:
    setk(&state[5]);
    break;
  case Qt::Key_Left:
    setk(&state[6]);
    break;
  case Qt::Key_Right:
    setk(&state[7]);
    break;
  }
  return QObject::eventFilter(obj, event);
}

void InputFilter::print_status() const {
  //   std::cout << a << b << select << start << up << down << left << right <<
  //   std::endl;
}
