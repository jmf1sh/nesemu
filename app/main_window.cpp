#include <fstream>
#include <iostream>
#include <string>

#include <QAction>
#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QPixmap>
#include <QStatusBar>

#include "main_window.hpp"
#include "tile.hpp"

static const int ppu_width = 256;
static const int ppu_height = 240;
static const int ppu_scale = 2;

const double nsec_per_frame = 16666666.6666667;

const nesemu::u32 g_palette[64] = {
    0x7C7C7C, 0x0000FC, 0x0000BC, 0x4428BC, 0x940084, 0xA80020, 0xA81000,
    0x881400, 0x503000, 0x007800, 0x006800, 0x005800, 0x004058, 0x000000,
    0x000000, 0x000000, 0xBCBCBC, 0x0078F8, 0x0058F8, 0x6844FC, 0xD800CC,
    0xE40058, 0xF83800, 0xE45C10, 0xAC7C00, 0x00B800, 0x00A800, 0x00A844,
    0x008888, 0x000000, 0x000000, 0x000000, 0xF8F8F8, 0x3CBCFC, 0x6888FC,
    0x9878F8, 0xF878F8, 0xF85898, 0xF87858, 0xFCA044, 0xF8B800, 0xB8F818,
    0x58D854, 0x58F898, 0x00E8D8, 0x787878, 0x000000, 0x000000, 0xFCFCFC,
    0xA4E4FC, 0xB8B8F8, 0xD8B8F8, 0xF8B8F8, 0xF8A4C0, 0xF0D0B0, 0xFCE0A8,
    0xF8D878, 0xD8F878, 0xB8F8B8, 0xB8F8D8, 0x00FCFC, 0xF8D8F8, 0x000000,
    0x000000};

MainWindow::MainWindow() : QMainWindow() {
  frame_data.resize(ppu_width * ppu_height);

  createActions();
  createStatusBar();

  scene = new QGraphicsScene(this);
  view = new QGraphicsView(scene, this);
  setScale(ppu_scale);

  statusBar()->setSizeGripEnabled(false);

  image = new QImage(ppu_width, ppu_height, QImage::Format_RGB32);
  image->setPixel(100, 100, 0x00FF0000);
  pixmap = scene->addPixmap(QPixmap::fromImage(*image));
  image->setPixel(120, 100, 0x00FF0000);
  image->setPixel(0, 0, 0x00FF0000);
  image->setPixel(255, 0, 0x00FF0000);
  image->setPixel(255, 239, 0x00FF0000);
  image->setPixel(0, 239, 0x00FF0000);
  pixmap->setPixmap(QPixmap::fromImage(*image));

  filter = new InputFilter(this);
  view->installEventFilter(filter);

  timer = new QTimer(this);
  timer->setTimerType(Qt::PreciseTimer);
  timer->setSingleShot(false);
  timer->setInterval(10); // 100 fps
  connect(timer, &QTimer::timeout, [this]() { this->step_frame(); });
}

void MainWindow::open() {
  timer->stop();

  QString fileName = QFileDialog::getOpenFileName(this);
  if (fileName.isEmpty()) {
    timer->start();
    return;
  }

  // todo...
  std::ifstream file(fileName.toStdString(),
                     std::ios_base::in | std::ios_base::binary);
  sys.rom.open(file);

  // todo: this should be handled by the cartridge mapper!
  for (auto i = 0; i < 0x1000; ++i) {
    sys.ppu.vram.pattern[0][i] = sys.rom.mapper->peek(i);
    sys.ppu.vram.pattern[1][i] = sys.rom.mapper->peek(i + 0x1000);
  }

  sys.ppu.vram.set_mirror_mode(sys.rom.mirror_mode);

  sys.reset();

  // testing
  nesemu::u8 bytes[16];
  for (nesemu::u16 sprite_id = 0; sprite_id < 32 * 16; ++sprite_id) {

    for (int i = 0; i < 16; ++i) {
      bytes[i] = sys.rom.mapper->peek(16 * sprite_id + i);
    }

    nesemu::Tile tile(bytes);

    int xoff = (sprite_id % 32) * 8;
    int yoff = (sprite_id / 32) * 8;

    for (int row = 0; row < 8; ++row) {
      for (int col = 0; col < 8; ++col) {
        image->setPixel(xoff + col, yoff + row,
                        0x00444444 * tile.pixels[row * 8 + col]);
      }
    }
  }
  pixmap->setPixmap(QPixmap::fromImage(*image));

  elapsed_timer.restart();
  elapsed = elapsed_timer.nsecsElapsed();

  timer->start();
}

void MainWindow::about() {
  QMessageBox::about(this, "About application",
                     "nesemu pre-alpha\n\nauthor: Jonathan Fisher");
}

void MainWindow::createActions() {
  QMenu *file_menu = menuBar()->addMenu(tr("&File"));

  QAction *act_open = new QAction(tr("&Open rom..."), this);
  act_open->setShortcuts(QKeySequence::Open);
  act_open->setStatusTip("Open a new rom");
  connect(act_open, &QAction::triggered, this, &MainWindow::open);
  file_menu->addAction(act_open);

  QAction *act_about =
      file_menu->addAction("&About nesapp", this, &MainWindow::about);
  act_about->setStatusTip("About nesapp");
  file_menu->addAction(act_about);
}

void MainWindow::createStatusBar() { statusBar()->showMessage("Ready"); }

void MainWindow::setScale(double scale) {
  int swidth = int(scale * ppu_width + 0.5);
  int sheight = int(scale * ppu_height + 0.5);

  view->setFrameRect(QRect(0, 0, ppu_width, ppu_height));
  view->setSceneRect(0, 0, ppu_width, ppu_height);
  const int pad = 10;
  const int yoff = menuBar()->height() + pad;
  view->setGeometry(pad, pad + yoff, swidth + pad, sheight + pad);
  view->resetTransform();
  view->scale(scale, scale);
  resize(swidth + 2 * pad, sheight + 2 * pad + yoff + statusBar()->height());
  setFixedSize(size());
}

void MainWindow::step_frame() {
  qint64 current_elapsed = elapsed_timer.nsecsElapsed();
  double delta = double(current_elapsed - elapsed);
  qint64 frames_to_render = qint64(delta / nsec_per_frame);

  if (frames_to_render < 1) {
    return;
  }

  sys.joypad.state[0] = filter->state;
  // sys.joypad.state[1] = filter->state;

  // filter->print_status();
  // std::cout << "Rendering " << frames_to_render << " frames" << std::endl;
  sys.step_frame();
  elapsed = current_elapsed;

  // debugging
  for (auto nt = 0; nt < 4; ++nt) {
    // std::cout << "nametable " << nt << std::endl;
    // sys.ppu.vram.print_nametable(nt);
  }

  for (auto row = 0; row < 240; ++row) {
    for (auto col = 0; col < 256; ++col) {
      auto index = row * ppu_width + col;
      frame_data[index] = g_palette[sys.ppu.fb[0][index]];
    }
  }

  QImage frame((const uchar *)&frame_data[0], ppu_width, ppu_height,
               QImage::Format_RGB32);
  pixmap->setPixmap(QPixmap::fromImage(frame));
}
