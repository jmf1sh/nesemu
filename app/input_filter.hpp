#ifndef INPUT_FILTER_HPP_INCLUDED
#define INPUT_FILTER_HPP_INCLUDED

#include <QEvent>
#include <QObject>

#include "types.hpp"

class InputFilter : public QObject {
  Q_OBJECT
public:
  InputFilter(QObject *parent);

  void print_status() const;

protected:
  bool eventFilter(QObject *obj, QEvent *event);

public:
  nesemu::JoyState state;
};

#endif
