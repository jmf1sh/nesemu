nesemu
================

author: Jonathan Fisher (jonathan.m.fisher@gmail.com, github.com/jmf1sh)

This is my (work-in-progress) attempt to implement a basic NES emulator.
All code is licensed under GPL3 unless otherwise specified. See LICENSE for details.

Build instructions
------------------
On *nix systems this is pretty simple. For now, all you need is a recent version of
cmake and a relatively up-to-date C++ compiler.
```
mkdir build && && cd build && cmake SOURCE_DIRECTORY && make && ctest
```
At some point, I will add a 3rd party dependency for graphics, sound, and joystick support.