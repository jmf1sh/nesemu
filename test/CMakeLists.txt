file(GLOB test_sources "*.cpp")
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/rom DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

foreach(source_file ${test_sources})
  get_filename_component(base_name ${source_file} NAME_WE)
  add_executable(${base_name} ${source_file} ${test_headers})
  target_link_libraries(${base_name} nes)
  add_test(${base_name} ${base_name})
endforeach(source_file)
