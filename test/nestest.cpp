#include "test.hpp"
#include "system.hpp"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

using namespace nesemu;

struct CPUState {
  u16 IP;
  u8 A, X, Y, P, SP;
  size_t cyc;
};

CPUState parse_line(const std::string& line) {
  CPUState status;
  using mode_type = decltype(std::hex);

  auto rd = [&](auto start, auto len, mode_type mode = std::hex) {
    std::stringstream str;
    std::string s = line.substr(start, len);
    str << mode << s;
    u32 result;
    str >> result;
    return result;
  };
  
  status.IP = rd(0, 4);
  status.A = rd(50, 2);
  status.X = rd(55, 2);
  status.Y = rd(60, 2);
  status.P = rd(65, 2);
  status.SP = rd(71, 2);
  status.cyc = rd(78, 3, std::dec);

  return status;
}

int main() {
  char buffer[256];
  std::ifstream log("rom/nestest.log", std::ios_base::in);
  size_t nlines = 0;
  System sys;
  std::ifstream nestest("rom/nestest.nes", std::ios_base::in|std::ios_base::binary);
  sys.rom.open(nestest);
  sys.cpu.reset();

  // Reset vector is at 0xC004, but log file starts at 0xC000 (works before PPU is ready)
  sys.cpu.IP = 0xC000;

  std::cout << std::hex << u16(sys.rom.mapper->prg_rom[0xFFFA - 0xC000]) << std::endl;
  std::cout << std::hex << u16(sys.rom.mapper->prg_rom[0xFFFB - 0xC000]) << std::endl;
  std::cout << std::hex << u16(sys.rom.mapper->prg_rom[0xFFFC - 0xC000]) << std::endl;
  std::cout << std::hex << u16(sys.rom.mapper->prg_rom[0xFFFD - 0xC000]) << std::endl;
  std::cout << std::hex << u16(sys.rom.mapper->prg_rom[0xFFFE - 0xC000]) << std::endl;
  std::cout << std::hex << u16(sys.rom.mapper->prg_rom[0xFFFF - 0xC000]) << std::endl;

  #define p(x) std::cout << std::hex << u16(x) << std::endl;

  p(sys.peek(0xC000));
  p(sys.peek(0xC001));
  p(sys.peek(0xC002));
  p(sys.peek(0xC003));

  std::cout << "0xfffa" << std::endl;
  p(sys.peek(0xFFFA));
  p(sys.peek(0xFFFB));
  p(sys.peek(0xFFFC));
  p(sys.peek(0xFFFD));
  p(sys.peek(0xFFFE));
  p(sys.peek(0xFFFD));

  while (!log.eof()) {
    log.getline(buffer, 256);
    std::string line(buffer);
    
    if (line.size() < 70) {
      break;
    }
    nlines++;
    CPUState status = parse_line(line);
    
    std::cout << line << std::endl; 

    EXPECT_EQ(u16(status.IP), u16(sys.cpu.IP));
    EXPECT_EQ(u16(status.SP), u16(sys.cpu.SP));
    EXPECT_EQ(u16(status.P), u16(sys.cpu.status.pack()));
    EXPECT_EQ(u16(status.A), u16(sys.cpu.A));
    EXPECT_EQ(u16(status.X), u16(sys.cpu.X));
    EXPECT_EQ(u16(status.Y), u16(sys.cpu.Y));
    EXPECT_EQ(u16(status.cyc), u16(sys.ppu.cyc()));

    sys.cpu.exec();
  }
  EXPECT_EQ(8991, nlines);
}
