#ifndef TEST_HPP_INCLUDED
#define TEST_HPP_INCLUDED

#include <cstdlib>
#include <iostream>
#include <string>

namespace test {

  class test_framework {
  private:
    size_t _success;
    size_t _failure;
  public:
    test_framework(): _success(0), _failure(0) {}
    
    ~test_framework() {
      std::cout << "--------------------------------\n";
      std::cout << _success << " success(es); " << _failure << " failure(s)" << std::endl;
      std::exit(_failure == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
    }

    template<typename T>
    void check(T&& t, const std::string& str, size_t lineno) {
      if (t) { ++_success; }
      else {
        ++_failure;
        std::cout << lineno << ": check '" << str << "' failed" << std::endl;
      }
    }

    template<typename T, typename R>
    void check_eq(T&& t, R&& r, const std::string& str, size_t lineno) {
      if (t == r) { ++_success; }
      else {
        ++_failure;
        std::cout << lineno << ": check '" << str << "' failed (" << t << " != " << r << ")" << std::endl;
      }
    }

    void check_throw(const std::string& str, size_t lineno, bool expect, std::string what) {
      ++_failure;
      std::cout << lineno << ": '" << str << "' ";
      if (expect) {
	std::cout << "did not throw";
      } else {
	std::cout << "threw ('" << what << "')";
      }
      std::cout << std::endl;
    }
  };

  test_framework g_test;
}

#define EXPECT_TRUE(condition) \
  ::test::g_test.check(condition, #condition " == true", __LINE__)

#define EXPECT_FALSE(condition) \
  ::test::g_test.check((condition) == false, #condition " == false", __LINE__)

#define EXPECT_EQ(a, b) \
  ::test::g_test.check_eq(a, b, #a " == " #b, __LINE__)

#define EXPECT_LESS(a, b) \
  ::test::g_test.check((a) < (b), #a " < " #b, __LINE__)

#define EXPECT_CLOSE(a, b, eps)                                 \
  ::test::g_test.check((a)-(b) < (eps) && (b)-(a) < (eps), "|" #a " - " #b "| < " #eps, __LINE__)

#define EXPECT_THROW(expr)						\
  do { bool threw = false; std::string what;  try { expr; } catch(std::exception& e) { threw = true; what = e.what();} \
    if (!threw) ::test::g_test.check_throw(#expr, __LINE__, true, what); } while(0);

#define EXPECT_NO_THROW(expr)						\
  do { bool threw = false; std::string what;  try { expr; } catch(std::exception& e) { threw = true; what = e.what();} \
    if (threw) ::test::g_test.check_throw(#expr, __LINE__, false, what); } while(0);


#endif
