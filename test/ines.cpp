#include "test.hpp"
#include "ines.hpp"
#include <fstream>

int main() {
  std::ifstream s;
  nesemu::NesRom rom;
  auto mode = std::fstream::in | std::fstream::binary;

  // invalid filename
  s.open("invalid_filename", mode);
  EXPECT_THROW(rom.open(s));

  // read test rom
  s.open("rom/nestest.nes");
  EXPECT_NO_THROW(rom.open(s));

  EXPECT_EQ(16*1024, rom.mapper->prg_rom.size());
  EXPECT_EQ(0*1024, rom.mapper->prg_ram.size());
  EXPECT_EQ(8*1024, rom.mapper->chr_rom.size());

  // check first instruction is correct
  EXPECT_EQ(0x4C, rom.mapper->prg_rom[0]);
  EXPECT_EQ(0xF5, rom.mapper->prg_rom[1]);
  EXPECT_EQ(0xC5, rom.mapper->prg_rom[2]);

  // mapper number
  EXPECT_EQ(0, rom.mapper_id);
  
  return EXIT_SUCCESS;
}
