#include "test.hpp"
#include "cpu.hpp"
#include "system.hpp"

using namespace nesemu;

int main() {
  #if 0
    System system;
    std::cout << "system started" << std::endl;

    system.cpu.IP = 0xC000;
    system.mmap.write_op(0xC000, 0x4C, 0xF5, 0xC5);
    EXPECT_EQ(system.cpu.disassemble(), "C000  4C F5 C5  JMP $C5F5");
    std::cout << system.cpu.disassemble() << "&&&&" << std::endl;
    
    system.cpu.IP = 0xC5F5;
    system.mmap.write_op(0xC5F5, 0xA2, 0x00);
    EXPECT_EQ(system.cpu.disassemble(), "C5F5  A2 00     LDX #$00");
    std::cout << system.cpu.disassemble() << "&&&&" << std::endl;
    #endif
    /*
    C000  4C F5 C5  JMP $C5F5                       A:00 X:00 Y:00 P:24 SP:FD CYC:  0
C5F5  A2 00     LDX #$00                        A:00 X:00 Y:00 P:24 SP:FD CYC:  9
C5F7  86 00     STX $00 = 00                    A:00 X:00 Y:00 P:26 SP:FD CYC: 15
C5F9  86 10     STX $10 = 00                    A:00 X:00 Y:00 P:26 SP:FD CYC: 24
C5FB  86 11     STX $11 = 00                    A:00 X:00 Y:00 P:26 SP:FD CYC: 33
C5FD  20 2D C7  JSR $C72D                       A:00 X:00 Y:00 P:26 SP:FD CYC: 42
C72D  EA        NOP                             A:00 X:00 Y:00 P:26 SP:FB CYC: 60
C72E  38        SEC                             A:00 X:00 Y:00 P:26 SP:FB CYC: 66
C72F  B0 04     BCS $C735                       A:00 X:00 Y:00 P:27 SP:FB CYC: 72
C735  EA        NOP                             A:00 X:00 Y:00 P:27 SP:FB CYC: 81
C736  18        CLC                             A:00 X:00 Y:00 P:27 SP:FB CYC: 87
C737  B0 03     BCS $C73C                       A:00 X:00 Y:00 P:26 SP:FB CYC: 93
C739  4C 40 C7  JMP $C740                       A:00 X:00 Y:00 P:26 SP:FB CYC: 99
C740  EA        NOP                             A:00 X:00 Y:00 P:26 SP:FB CYC:108
C741  38        SEC                             A:00 X:00 Y:00 P:26 SP:FB CYC:114
C742  90 03     BCC $C747                       A:00 X:00 Y:00 P:27 SP:FB CYC:120
C744  4C 4B C7  JMP $C74B                       A:00 X:00 Y:00 P:27 SP:FB CYC:126
C74B  EA        NOP                             A:00 X:00 Y:00 P:27 SP:FB CYC:135
C74C  18        CLC                             A:00 X:00 Y:00 P:27 SP:FB CYC:141
C74D  90 04     BCC $C753                       A:00 X:00 Y:00 P:26 SP:FB CYC:147
C753  EA        NOP                             A:00 X:00 Y:00 P:26 SP:FB CYC:156
C754  A9 00     LDA #$00                        A:00 X:00 Y:00 P:26 SP:FB CYC:162
C756  F0 04     BEQ $C75C                       A:00 X:00 Y:00 P:26 SP:FB CYC:168
C75C  EA        NOP                             A:00 X:00 Y:00 P:26 SP:FB CYC:177
C75D  A9 40     LDA #$40                        A:00 X:00 Y:00 P:26 SP:FB CYC:183
C75F  F0 03     BEQ $C764                       A:40 X:00 Y:00 P:24 SP:FB CYC:189
C761  4C 68 C7  JMP $C768                       A:40 X:00 Y:00 P:24 SP:FB CYC:195
C768  EA        NOP                             A:40 X:00 Y:00 P:24 SP:FB CYC:204
C769  A9 40     LDA #$40                        A:40 X:00 Y:00 P:24 SP:FB CYC:210
    */

    return EXIT_SUCCESS;
}
